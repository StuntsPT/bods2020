#### Natural Selection Detection Revisited – Challenges and Insights From Automating and Massively Scaling the Process

###### Bioinformatics Open Days 2020

</br>

<center>Francisco Pina Martins</center>
<center>February 2020</center>

[@FPinaMartins](https://twitter.com/FPinaMartins)

<img src="assets/cover_logos.png" style="background:none; border:none; box-shadow:none;" alt="FCUL, cE3c and CoBiG² logos" style="text-align: center" />

---

### What is [Natural] Selection?

<ul>
<li class="fragment">A mechanism of evolution</li>
  <ul>
  <li class="fragment">There is variation in **phenotypes**</li>
  <li class="fragment">Phenotypes have **differential reproduction**</li>
  <li class="fragment">There is heritability</li>
  <li class="fragment">More **advantageous** phenotypes increase in frequency</li>
  </ul>
</ul>

---

### What is [Natural] Selection?

<img src="assets/Selection.png" alt="Selection plot"  style="background:none; border:none; box-shadow:none;" />

---

### How can selection be detected?
<style>
.container{
display: flex;
}
.col{
flex: 1;
}
</style>
<div class="container">
<div class="col">
<div class="fragment">
Outlier detection
<img src="assets/KLDs.png" alt="KLD Manhattan plot" style="background:none; border:none; box-shadow:none;" />
</br>
Finds alleles with "unexpected" frequencies
<ul>
<li class="fragment">Bayescan</li>
<li class="fragment">SelEstim</li>
</ul>
</div>
</div>

<div class="col">
<div class="fragment">
Association with variable

<img src="assets/Association.png" alt="Correlation plot" style="background:none; border:none; box-shadow:none;" />
</br>
Finds alleles that vary with external variables
<ul>
<li class="fragment">Baypass</li>
<li class="fragment">LFMM</li>
</ul>
</div>

</div>

---

### Current methods limitations

<ul>
<li class="fragment" data-fragment-index="1">[Number of false positives](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14584)</li>
<div class="fragment" data-fragment-index="1" style="float: right"><img src="assets/type-i-error.jpg" alt="Spheres" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="2">[Single locus](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14584)</li>
<div class="fragment" data-fragment-index="2" style="float: right"><img src="assets/Hexlet_800.gif" alt="Spheres" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="3">[CORRELATION DOES NOT MEAN CAUSATION!](https://www.tylervigen.com/spurious-correlations)</li>
<div class="fragment" data-fragment-index="3" style="float: left"><img src="assets/spurious.png" alt="Spurious correlation plot" style="background:none; border:none; box-shadow:none;"/></div>
</ul>

---

### Methods in wide usage

<ul>
<li class="fragment">Thousands of papers rely on these methods</li>
  <ul>
  <li class="fragment">[1721 citations for Bayescan alone](https://scholar.google.pt/scholar?cites=4319645506146909873&as_sdt=2005&sciodt=0,5&hl=pt-PT)</li>
  </ul>
<li class="fragment">This led to some interesting meta-analyses</li>
  <ul>
  <li class="fragment">[Ahrens et al. 2018](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14549)</li>
  </ul>
</ul>

<img src="assets/ahrens-01.png" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### The importance of detection selection

<ul>
<li class="fragment">Understand Evolutionary History</li>
<li class="fragment">Understand phenotypic diversity</li>
<li class="fragment">Understand the response of adaptive traits</li>
  <ul>
  <li class="fragment">Ultimately required to understand how species can adapt to new conditions</li>
  </ul>
</ul>

<img src="assets/owls.jpg" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### The problem thus far...

<ul>
<li class="fragment">Lack of standard method</li>
<li class="fragment">Direct comparisons not possible</li>
  <ul>
  <li class="fragment">Within the same method</li>
  <li class="fragment">Between different methods</li>
  <li class="fragment">No baseline</li>
  </ul>
<li class="fragment">A lot of (good) work is being done, but it's hard to combine all the generated information!</li>
</ul>

<img src="assets/incomparable.png" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### Goals:

<ul>
<li class="fragment">Understand how much results depend on methods</li>
  <ul>
  <li class="fragment">Comparing different methodologies</li>
  <li class="fragment">Comparing parameter ranges</li>
  </ul>
<li class="fragment">Effectively defining a baseline</li>
</ul>

<img src="assets/orpple.png" class="fragment" style="background:none; border:none; box-shadow:none;" alt="Apple and orange 'hybrid'" />

---

### How to achieve them? (I)

<ul>
<li class="fragment" data-fragment-index="1">Re-analyzing datasets from the literature</li>
  <ul>
  <li class="fragment" data-fragment-index="2">List of 46 datasets identified so far</li>
  </ul>
<li class="fragment" data-fragment-index="3">Requirements:</li>
  <ul>
  <li class="fragment" data-fragment-index="4">SNP data available (VCF)</li>
  <li class="fragment" data-fragment-index="5">Sample coordinates available</li>
  </ul>
</ul>

<img src="assets/single_miner.jpg" class="fragment" alt="Surprised miner" style="float: left; background:none; border:none; box-shadow:none;" data-fragment-index="6" />
<img src="assets/Minions.png" class="fragment" alt="My minions" style="float: center; background:none; border:none; box-shadow:none;" data-fragment-index="8" />
<img src="assets/miners.jpg" class="fragment" alt="Prisioners mining" style="float: right; background:none; border:none; box-shadow:none;" data-fragment-index="7" />

---

### How to achieve them? (II)

<ul>
<li class="fragment">Standardization</li>
  <ul>
  <li class="fragment">All datasets are analysed in the same way</li>
  </ul>
<li class="fragment">Automation</li>
  <ul>
  <li class="fragment">All analyses performed with a single command</li>
  </ul>
<li class="fragment">Reproducibility</li>
  <ul>
  <li class="fragment">[GNU Make](https://www.gnu.org/software/make/)</li>
  </ul>
<li class="fragment">Controlled environment</li>
  <ul>
  <li class="fragment">[Docker](https://www.docker.com/)</li>
  </ul>
</ul>

<a href="https://gitlab.com/StuntsPT/selectiondetection"><img src="assets/gitlab.png" class="fragment" alt="Gitlab logo" style="background:none; border:none; box-shadow:none;" /></a>

---

### Hands on

<a href="assets/selectiondetection.png"><img src="assets/selectiondetection.png" class="fragment" style="background:none; border:none; box-shadow:none;" alt="selection detection pipeline diagram" /></a>

---

### Challenges so far

<ul>
<li class="fragment">Marine organisms</li>
  <ul>
  <li class="fragment">Need a new set of climatic variables</li>
  </ul>
<li class="fragment">Parameter range</li>
  <ul>
  <li class="fragment">Need to be inclusive, but meaningful</li>
  </ul>

<img src="assets/challange.jpg" class="fragment" style="background:none; border:none; box-shadow:none;" alt="Challange accepted" style="text-align: center" />

---

### Results thus far

<ul>
<li class="fragment">6 datasets analysed</li>
<li class="fragment">~24h run time each</li>
  <ul>
  <li class="fragment">CPU limited</li>
  <li class="fragment">*SelEstim* takes ~80% of the time</li>
  <li class="fragment">Can be improved by parameter exploration</li>
  </ul>
</ul>

<img src="assets/cloud.png" class="fragment" style="background:none; border:none; box-shadow:none;" alt="Challange accepted" style="text-align: center" />

---

### Future directions

<ul>
<li class="fragment" data-fragment-index="1" >More programs</li>
<div class="fragment" data-fragment-index="6" style="float: right"><img src="assets/Esteves.jpeg" alt="André Esteves" style="background:none; border:none; box-shadow:none;"/></div>
  <ul>
  <li class="fragment" data-fragment-index="2" >[OUTflank](https://github.com/whitlock/OutFLANK)</li>
  <li class="fragment" data-fragment-index="2" >[PCAdapt](https://doi.org/10.1111/1755-0998.12592)</li>
  <li class="fragment" data-fragment-index="2" >[RDA](https://doi.org/10.1111/mec.14584)</li>
  </ul>
<li class="fragment" data-fragment-index="3" >Marine environmental variable map data</li>
<li class="fragment" data-fragment-index="4" >MOAR DATA!</li>
<li class="fragment" data-fragment-index="5" >Meta wrapper - to create meta-plots</li>
</ul>

---

### Acknowledgements


<img src="assets/Acknowledgements.png" style="background:none; border:none; box-shadow:none;" alt="Acknowledgements" style="text-align: center" />
